# README #

# Empty template for develop on Vue #
### Run this command to install all dependencies for project: ###

```
#!bash

$ npm i
```


### Run this command to start build Vue-project which contain Vue-components with their styles and business logic: ###

```
#!bash

$ npm run build
```

### If you see this then all is fine and your project has been successfully build ###
![Bash](https://bitbucket.org/repo/8p5xar/images/2676824950-%D0%91%D0%B5%D0%B7%D1%8B%D0%BC%D1%8F%D0%BD%D0%BD%D1%8B%D0%B9.png)